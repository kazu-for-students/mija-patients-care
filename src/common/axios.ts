import i18n from "../i18n";
import axios from "axios";

export const axiosConfig = axios.interceptors.request.use(
  function (config) {
    config.baseURL = process.env.REACT_APP_API_URL;
    config.headers!["Accept-Language"] = i18n.language;
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default axiosConfig;
