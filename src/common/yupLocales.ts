import * as yup from "yup";
import i18n from "i18n";

yup.setLocale({
  mixed: {
    required: () => i18n.t("validation:required"),
  },
  string: { email: () => i18n.t("validation:invalidEmail") },
});

export default yup;
