import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#00ABB3",
      contrastText: "rgba(255,255,255,0.87)",
    },
    secondary: {
      main: "#f3f3f3",
    },
    text: {
      primary: "#1e1e1e",
      secondary: "#3b3b3b",
    },
    background: {
      default: "#f3f3f3",
      paper: "#EAEAEA",
    },
    error: {
      main: "#f43a36",
    },
    warning: {
      main: "#ffb000",
    },
    info: {
      main: "#2493ec",
    },
    success: {
      main: "#4db551",
    },
    divider: "rgba(6,5,5,0.12)",
  },
});

export type ThemeType = typeof theme;
