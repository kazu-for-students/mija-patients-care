import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`  
* {
  box-sizing: border-box;
}

body {
  margin: 0;
  padding: 0;
  color: ${({ theme }) => theme.palette.text.primary};
  background-color: ${({ theme }) => theme.palette.background.paper};
  font-family: ${({ theme }) => theme.typography.fontFamily};
  font-weight: 400;
}

a {
  color:${({ theme }) => theme.palette.primary.main};
  text-decoration: none;
  font-weight:bold;
}

#root {
    min-height: 100vh;
    display: flex;
    }
`;
export default GlobalStyle;
