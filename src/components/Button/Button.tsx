import { useState } from "react";
import { StyledCircularProgress } from "./Button.styled";
import { ButtonProps } from "./Button.type";
import { default as ButtonMui } from "@mui/material/Button";


const Button = ({
  children,
  onClick,
  variant = "contained",
  disabled,
  loading,
  onClickAsync,
  ...otherProps
}: ButtonProps) => {
  const [isAsyncCLickLoading, setIsAsyncCLickLoading] = useState(false);

  const handleOnClick: React.MouseEventHandler<HTMLButtonElement> = async (event) => {
    if (onClick) onClick(event);
    if (onClickAsync) {
      setIsAsyncCLickLoading(true);
      await onClickAsync(event);
      setIsAsyncCLickLoading(false);
    }
  };

  return (
    <ButtonMui
      variant={variant}
      disabled={disabled ? disabled : isAsyncCLickLoading || loading}
      onClick={handleOnClick}
      {...otherProps}
    >
      <>
        {(isAsyncCLickLoading || loading) && <StyledCircularProgress size={24} />} {children}
      </>
    </ButtonMui>
  );
};

export default Button;
