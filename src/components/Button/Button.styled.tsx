import styled from "styled-components";
import { CircularProgress } from "@mui/material";

export const StyledCircularProgress = styled(CircularProgress)`
  position: absolute;
  color: ${({ theme }) => theme.palette.primary.main};
`;
