import { ButtonProps as ButtonMuiProps } from "@mui/material/Button";

export interface ButtonProps extends ButtonMuiProps {
  loading?: boolean;
  onClickAsync?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => Promise<void>;
}
