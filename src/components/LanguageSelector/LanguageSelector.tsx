import { useTranslation } from "react-i18next";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import TranslateIcon from "@mui/icons-material/Translate";
import { ReactComponent as PlFlag } from "./flags/pl.svg";
import { ReactComponent as DeFlag } from "./flags/de.svg";
import { ReactComponent as UsFlag } from "./flags/us.svg";
import { useState } from "react";
import { ListItemText, Tooltip } from "@mui/material";
import { StyledListItemIcon } from "./LanguageSelector.styled";

const LanguageSelector = () => {
  const { i18n, t } = useTranslation(["dashboard"]);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleChangeLanguage = (language: string) => {
    i18n.changeLanguage(language);
    setAnchorEl(null);
  };

  const languages = [
    {
      name: "Polski",
      shortName: "pl",
      flag: <PlFlag />,
    },
    {
      name: "English",
      shortName: "en",
      flag: <UsFlag />,
    },
    {
      name: "Deutsch",
      shortName: "de",
      flag: <DeFlag />,
    },
  ];

  return (
    <>
      <Tooltip title={t("chooseLng")}>
        <Button onClick={handleClick}>
          <TranslateIcon />
        </Button>
      </Tooltip>

      <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
        {languages.map(({ name, shortName, flag }) => (
          <MenuItem key={shortName} onClick={() => handleChangeLanguage(shortName)}>
            <StyledListItemIcon>{flag}</StyledListItemIcon>
            <ListItemText>{name}</ListItemText>
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export default LanguageSelector;
