import { styled } from "@mui/system";
import { ListItemIcon } from "@mui/material";


export const StyledListItemIcon = styled(ListItemIcon)`
  padding-right: 1rem;
`;
