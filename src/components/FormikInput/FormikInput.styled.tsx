import styled from "styled-components";

export const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const StyledError = styled.p`
  color: ${({ theme }) => theme.palette.error.main};
  margin-top: 4px;
  margin-right: 14px;
  margin-bottom: 0;
  margin-left: 14px;
  font-size: 0.75rem;
  font-weight: 400;
  line-height: 1.66;
  text-align: left;
`;
