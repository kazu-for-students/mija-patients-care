import { BaseTextFieldProps, IconButton, InputAdornment } from "@mui/material";
import { VisibilityOff, Visibility } from "@mui/icons-material";
import { TextField } from "@mui/material";
import {
  Field,
  FieldProps,
  FormikValues,
  FormikConfig,
  FieldConfig,
  FieldInputProps,
} from "formik";
import { HTMLInputTypeAttribute, useState } from "react";
import { StyledError, StyledWrapper } from "./FormikInput.styled";

export interface FormikInputProps {
  name: string;
  label: string;
  textFieldProps?: Omit<
    BaseTextFieldProps,
    | keyof FormikConfig<FormikValues>
    | keyof FieldConfig<FormikValues>
    | keyof FieldInputProps<FormikValues>
  >;
  type?: HTMLInputTypeAttribute;
}

const FormikInput = ({ name, label, textFieldProps, type = "text" }: FormikInputProps) => {
  const [isPasswordShow, setIsPasswordShow] = useState(false);
  const [typeValue, setTypeValue] = useState(type);

  const hideToggleHandler = () => {
    setIsPasswordShow((prev) => !prev);
    setTypeValue((currentType) => (currentType === "password" ? "text" : "password"));
  };
  return (
    <Field name={name}>
      {({ field, meta }: FieldProps) => (
        <StyledWrapper>
          <TextField
            {...field}
            label={label}
            type={typeValue}
            size={"small"}
            error={Boolean(meta.error)}
            InputProps={{
              endAdornment: type === "password" && (
                <InputAdornment position="end">
                  <IconButton onClick={() => hideToggleHandler()}>
                    {isPasswordShow ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
            {...textFieldProps}
          />
          {meta.touched && meta.error && <StyledError>{meta.error}</StyledError>}
        </StyledWrapper>
      )}
    </Field>
  );
};

export default FormikInput;
