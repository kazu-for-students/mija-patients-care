import styled from "styled-components";
import { Form } from "formik";
import { Container } from "@mui/material";

const BASICLAYOUT_MINWIDTH = 375;
export const StyledContainer = styled(Container)`
  display: flex;
  flex-direction: column;
  background-color: ${({ theme }) => theme.palette.background.default};
  gap: 1rem;
  padding: 1rem;
  text-align: center;
  border-radius: 3px;
  width: unset;
  min-width: ${BASICLAYOUT_MINWIDTH + "px"};
  align-self: center;
  ${({ theme }) => theme.breakpoints.down(BASICLAYOUT_MINWIDTH)} {
    min-width: unset;
  }
`;

export const TopWrapper = styled.div`
  margin-right: 100%;
`;
export const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
  gap: 2rem;
`;
export const StyledFooter = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem;
  font-size: 0.8rem;
  gap: 0.5rem;
`;
