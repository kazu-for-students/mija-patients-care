import { StyledContainer, TopWrapper } from "./BasicLayout.styled";
import LanguageSelector from "components/LanguageSelector";
import { Typography } from "@mui/material";

export interface BasicLayoutProps {
  title?: string;
  children: React.ReactNode;
}

const BasicLayout = ({ title, children }: BasicLayoutProps) => (
  <>
    <StyledContainer fixed>
      <TopWrapper>
        <LanguageSelector />
      </TopWrapper>
      {title && (
        <Typography component="h1" variant="h6">
          {title}
        </Typography>
      )}
      {children}
    </StyledContainer>
  </>
);

export default BasicLayout;
