import styled from "styled-components";
import Container from "@mui/material/Container";
import AppBar from "@mui/material/AppBar";
import { TOPBAR_HEIGHT } from "constants/layoutConstants";
import { Button, Typography } from "@mui/material";

export const StyledContainer = styled(Container)`
  padding: 1rem;
  height: ${TOPBAR_HEIGHT}px;
`;
export const StyledWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`;
export const StyledAppBar = styled(AppBar)`
  border-bottom: 1px solid ${({ theme }) => theme.palette.divider};
  box-shadow: unset;
`;
export const StyledButton = styled(Button)`
  ${({ theme }) => theme.breakpoints.down("xs")} {
    min-width: 45px;
  }
`;
export const StyledTypography = styled(Typography)`
  margin: 0 0.5rem 0 1rem;
  font-size: 0.8rem;
  font-weight: 700;
  text-decoration: "none";
`;
export const StyledToolWrapper = styled.div`
  margin: 0px 2rem;
  border-right: 1px solid ${({ theme }) => theme.palette.primary.main};
  border-left: 1px solid ${({ theme }) => theme.palette.primary.main};
  ${({ theme }) => theme.breakpoints.down("sm")} {
    margin: 0px 1rem;
  }
`;
