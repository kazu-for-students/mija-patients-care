import * as React from "react";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import LogoutIcon from "@mui/icons-material/Logout";
import Avatar from "@mui/material/Avatar";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import SettingsIcon from "@mui/icons-material/Settings";
import LanguageSelector from "components/LanguageSelector";
import {
  StyledAppBar,
  StyledButton,
  StyledContainer,
  StyledToolWrapper,
  StyledTypography,
  StyledWrapper,
} from "./Topbar.styled";
import { useTranslation } from "react-i18next";
import { Divider } from "@mui/material";

export type settingsListType = string[];
export type UserData = { name: string; avatarUrl: string };

export interface TopbarProps {
  userData: UserData;
  settingsList: settingsListType;
}
//for now its test/dummy type and interface
const Topbar = ({ userData, settingsList }: TopbarProps) => {
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);
  const { t } = useTranslation(["dashboard"]);

  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <StyledAppBar position="fixed" color="secondary">
      <StyledContainer maxWidth={false}>
        <StyledWrapper>
          <StyledToolWrapper>
            <Tooltip title={t("openSettings")}>
              <StyledButton onClick={handleOpenUserMenu}>
                <SettingsIcon />
              </StyledButton>
            </Tooltip>
            <LanguageSelector />
            <Tooltip title={t("logout")}>
              <StyledButton>
                <LogoutIcon />
              </StyledButton>
            </Tooltip>
          </StyledToolWrapper>
          <Avatar
            sx={{
              display: { xs: "none", sm: "flex" },
            }}
            alt={userData.name}
            src={userData.avatarUrl}
          />
          <StyledTypography
            variant="h6"
            noWrap
            sx={{
              display: { xs: "none", md: "flex" },
            }}
          >
            {userData.name}
          </StyledTypography>
          <Menu
            anchorEl={anchorElUser}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            open={Boolean(anchorElUser)}
            onClose={handleCloseUserMenu}
          >
            {settingsList.map((setting, index) => (
              <div key={index}>
                {settingsList.length - 1 === index && <Divider />}
                <MenuItem  onClick={handleCloseUserMenu}>
                  <Typography textAlign="center">{setting}</Typography>
                </MenuItem>
              </div>
            ))}
          </Menu>
        </StyledWrapper>
      </StyledContainer>
    </StyledAppBar>
  );
};
export default Topbar;
