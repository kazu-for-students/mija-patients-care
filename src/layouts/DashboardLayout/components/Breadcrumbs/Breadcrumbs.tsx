import { Typography } from "@mui/material";
import { Breadcrumbs as BreadcrumbsMui } from "@mui/material";
import { Link } from "react-router-dom";

export interface BreadcrumbsProps {
  breadcrumbslinksMap: {
    label: string;
    link?: string;
  }[];
}

const Breadcrumbs = ({ breadcrumbslinksMap }: BreadcrumbsProps) => {
  return (
    <BreadcrumbsMui aria-label="breadcrumb">
      {breadcrumbslinksMap.map(({ label, link }, index) => {
        const isLast = index === breadcrumbslinksMap.length - 1;
        return isLast ? (
          <Typography key={index}>{label}</Typography>
        ) : (
          <Link key={index} color="inherit" to={link!}>
            {label}
          </Link>
        );
      })}
    </BreadcrumbsMui>
  );
};

export default Breadcrumbs;
