import { ExpandLess, ExpandMore } from "@mui/icons-material";
import { Button, Collapse, Toolbar, Tooltip } from "@mui/material";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import MenuIcon from "@mui/icons-material/Menu";
import { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import { StyledDrawer, StyledHamburgerBox, StyledLogoWrapper, StyledText } from "./Sidebar.styled";
import { TOPBAR_HEIGHT } from "constants/layoutConstants";

export interface SidebarLinksInterface {
  title: string;
  icon?: JSX.Element;
  path?: string;
  dropdownLinks?: { title: string; path: string }[];
}

export interface SidebarProps {
  sidebarLinks: SidebarLinksInterface[];
  logo?: JSX.Element;
}

const Sidebar = ({ sidebarLinks, logo }: SidebarProps) => {
  const [oppenedDropdown, setOppenedDropdown] = useState<number[]>([]);
  const [mobileOpen, setMobileOpen] = useState(false);
  const { pathname } = useLocation();

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleDropdownClick = (dropdownIndex: number) => {
    if (oppenedDropdown.includes(dropdownIndex)) {
      setOppenedDropdown(oppenedDropdown.filter((oppenedIndex) => oppenedIndex !== dropdownIndex));
      return;
    }
    setOppenedDropdown([...oppenedDropdown, dropdownIndex]);
  };

  useEffect(() => {
    sidebarLinks.forEach(({ dropdownLinks }, index) => {
      if (dropdownLinks && dropdownLinks.some(({ path }) => pathname === path)) {
        setOppenedDropdown([index]);
      }
    });
  }, [pathname, sidebarLinks]);

  return (
    <>
      <StyledHamburgerBox sx={{ display: { xs: "block", sm: "none" } }}>
        <Toolbar sx={{ height: TOPBAR_HEIGHT }}>
          <Tooltip title="Sidebar">
            <Button onClick={handleDrawerToggle}>
              <MenuIcon />
            </Button>
          </Tooltip>
        </Toolbar>
      </StyledHamburgerBox>

      <Box sx={{ display: { xs: mobileOpen ? "flex" : "none", sm: "flex" } }}>
        <StyledDrawer
          color="secondary"
          variant={mobileOpen ? "temporary" : "permanent"}
          onClose={handleDrawerToggle}
          open={mobileOpen}
        >
          <StyledLogoWrapper>{logo}</StyledLogoWrapper>

          <Box sx={{ overflow: "auto" }}>
            <List>
              {sidebarLinks.map(({ icon, title, path, dropdownLinks }, index) => {
                if (dropdownLinks) {
                  return (
                    <div key={index}>
                      <ListItemButton onClick={() => handleDropdownClick(index)}>
                        <ListItemIcon>{icon}</ListItemIcon>
                        <StyledText primary={title} />
                        {oppenedDropdown.includes(index) ? <ExpandLess /> : <ExpandMore />}
                      </ListItemButton>

                      <Collapse in={oppenedDropdown.includes(index)} timeout="auto" unmountOnExit>
                        {dropdownLinks.map(({ path, title }, index) => {
                          const isSelected = pathname === path;
                          return (
                            <Link key={index} to={path} onClick={() => setMobileOpen(false)}>
                              <ListItem disablePadding>
                                <ListItemButton selected={isSelected} sx={{ pl: 4 }}>
                                  <StyledText primary={title} />
                                </ListItemButton>
                              </ListItem>
                            </Link>
                          );
                        })}
                      </Collapse>
                    </div>
                  );
                }
                return (
                  <Link key={index} to={path!} onClick={() => setMobileOpen(false)}>
                    <ListItem selected={pathname === path} disablePadding>
                      <ListItemButton>
                        <ListItemIcon>{icon}</ListItemIcon>
                        <StyledText primary={title} />
                      </ListItemButton>
                    </ListItem>
                  </Link>
                );
              })}
            </List>
          </Box>
        </StyledDrawer>
      </Box>
    </>
  );
};

export default Sidebar;
