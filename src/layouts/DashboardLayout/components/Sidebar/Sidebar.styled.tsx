import styled from "styled-components";
import Container from "@mui/material/Container";
import { SIDEBAR_WIDTH, TOPBAR_HEIGHT } from "constants/layoutConstants";
import { Box, Drawer, ListItemText } from "@mui/material";


export const StyledContainer = styled(Container)`
  padding: 1rem;
`;
export const StyledDrawer = styled(Drawer)`
  width: ${SIDEBAR_WIDTH}px;
  flex-shrink: 0;
  .MuiDrawer-paper {
    width: ${SIDEBAR_WIDTH}px;
    background-color: ${({ theme }) => theme.palette.background.default};
  }
`;
export const StyledText = styled(ListItemText)`
  color: ${({ theme }) => theme.palette.text.secondary};
`;
export const StyledHamburgerBox = styled(Box)`
  position: fixed;
  margin-right: 2px;
  z-index: ${({ theme }) => theme.zIndex.appBar + 1};
`;
export const StyledWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const StyledLogoWrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 1rem;
  height: ${TOPBAR_HEIGHT}px;
`;
