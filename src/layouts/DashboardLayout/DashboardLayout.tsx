import Sidebar from "./components/Sidebar";
import { SidebarLinksInterface } from "./components/Sidebar";
import { StyledContentWrapper, StyledWrapper } from "./DashboardLayout.styled";
import Topbar, { settingsListType, UserData } from "./components/Topbar";
import Breadcrumbs, { BreadcrumbsProps } from "./components/Breadcrumbs";

export interface DashbordProps extends BreadcrumbsProps {
  children: React.ReactNode;
  userData: UserData;
  sidebarNav: SidebarLinksInterface[];
  logo?: JSX.Element;
  topbar: settingsListType;
}
const DashboardLayout = ({
  children,
  userData,
  sidebarNav,
  breadcrumbslinksMap,
  logo,
  topbar,
}: DashbordProps) => (
  <>
    <Topbar settingsList={topbar} userData={userData} />
    <Sidebar sidebarLinks={sidebarNav} logo={logo} />
    <StyledWrapper>
      <Breadcrumbs breadcrumbslinksMap={breadcrumbslinksMap} />
      <StyledContentWrapper> {children}</StyledContentWrapper>
    </StyledWrapper>
  </>
);

export default DashboardLayout;
