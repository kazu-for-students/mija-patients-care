import { TOPBAR_HEIGHT } from "constants/layoutConstants";
import styled from "styled-components";

export const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: ${TOPBAR_HEIGHT}px;
  padding: 1rem;
  width: 100%;
`;

export const StyledContentWrapper = styled.div`
  height: 100%;
  padding: 1rem;
  border: 1px solid ${({ theme }) => theme.palette.divider};
  margin: 1rem;
  background-color: ${({ theme }) => theme.palette.background.default};
`;
