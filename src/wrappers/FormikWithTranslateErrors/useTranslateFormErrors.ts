import { FormikErrors, FormikTouched, FormikValues } from "formik";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";

export interface useTranslateFormErrorsProps {
  errors: FormikErrors<FormikValues>;
  touched: FormikTouched<FormikValues>;
  setFieldTouched: (field: string, isTouched?: boolean, shouldValidate?: boolean) => void;
}

const useTranslateFormErrors = ({
  errors,
  touched,
  setFieldTouched,
}: useTranslateFormErrorsProps) => {
  const { i18n } = useTranslation();
  useEffect(() => {
    Object.keys(errors).forEach((fieldName) => {
      if (Object.keys(touched).includes(fieldName)) {
        setFieldTouched(fieldName);
      }
    });
  }, [errors, i18n.language, setFieldTouched, touched]);
};

export default useTranslateFormErrors;
