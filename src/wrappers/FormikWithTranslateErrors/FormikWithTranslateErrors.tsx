import { Formik, FormikConfig, FormikValues } from "formik";
import useTranslateFormErrors, { useTranslateFormErrorsProps } from "./useTranslateFormErrors";

interface TranslateFormErrorsProps {
  form: useTranslateFormErrorsProps;
  children: React.ReactNode;
}

export interface FormikWithTranslateErrorsProps {
  children: React.ReactNode;
  formikConfig: FormikConfig<FormikValues>;
}

const TranslateFormErrors = ({ form, children }: TranslateFormErrorsProps) => {
  useTranslateFormErrors(form);
  return <>{children}</>;
};

const FormikWithTranslateErrors = ({ formikConfig, children }: FormikWithTranslateErrorsProps) => {
  return (
    <Formik {...formikConfig}>
      {({ errors, touched, setFieldTouched }) => (
        <TranslateFormErrors form={{ errors, touched, setFieldTouched }}>
          {children}
        </TranslateFormErrors>
      )}
    </Formik>
  );
};

export default FormikWithTranslateErrors;
