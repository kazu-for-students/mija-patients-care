import PersonIcon from "@mui/icons-material/Person";
import DataUsageIcon from "@mui/icons-material/DataUsage";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { SidebarLinksInterface } from "layouts/DashboardLayout/components/Sidebar/Sidebar";
import DashboardLayout from "layouts/DashboardLayout";
import { ReactComponent as Logo } from "assets/logo.svg";
import { BreadcrumbsProps } from "layouts/DashboardLayout/components/Breadcrumbs/Breadcrumbs";
import { settingsListType } from "layouts/DashboardLayout/components/Topbar/Topbar";

export interface DashboardLayoutWrapperProps extends BreadcrumbsProps {
  children: React.ReactNode;
}

const DashboardLayoutWrapper = ({ breadcrumbslinksMap, children }: DashboardLayoutWrapperProps) => {
  const sidebarLinks: SidebarLinksInterface[] = [
    {
      title: "Patient",
      icon: <PersonIcon />,
      dropdownLinks: [
        { title: "List of Patients", path: "/dashboard/patientlist" },
        { title: "View Patient", path: "/dashboard/TestVew" },
        { title: "Edit Patient", path: "/dashboard/patientedit" },
      ],
    },
    {
      title: "Statistic",
      icon: <DataUsageIcon />,
      dropdownLinks: [
        { title: "Data", path: "/dashboard/data" },
        { title: "Edit Data", path: "/dashboard/TestVew/test" },
      ],
    },
    { title: "Products", icon: <ShoppingCartIcon />, path: "/dashboard/testView" },
  ];
  const dummyUserData = {
    name: "Michal Japko",
    avatarUrl: "/static/images/avatar/2.jpg",
  };

  const settingsList: settingsListType = ["Edit Profile", "Edit smth", "Logout"];

  return (
    <DashboardLayout
      userData={dummyUserData}
      sidebarNav={sidebarLinks}
      breadcrumbslinksMap={breadcrumbslinksMap}
      logo={<Logo />}
      topbar={settingsList}
    >
      {children}
    </DashboardLayout>
  );
};

export default DashboardLayoutWrapper;
