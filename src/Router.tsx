import NotFound from "components/NotFound/NotFound";
import DashboardView from "feature/dashboard/views/DashboardView";
import BaseLoggedView from "feature/user/view/BaseLoggedView";
import LoginView from "feature/user/view/LoginView";
import { Route, Routes } from "react-router-dom";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<LoginView />} />
      <Route path="dashboard" element={<BaseLoggedView />} />
      <Route path="/dashboard/TestVew" element={<DashboardView />} />
      <Route path="/dashboard/TestVew/test" element={<DashboardView />} />
      <Route path="*" element={<NotFound />} />;
    </Routes>
  );
};

export default Router;
