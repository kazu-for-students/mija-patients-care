import { createSlice } from "@reduxjs/toolkit";

import { RootState } from "store/store";

const initialState: {
  userAuth: boolean | null;
} = {
  userAuth: true,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
});

export const selectUserAuth = (state: RootState) => state.user.userAuth;

export default userSlice.reducer;
