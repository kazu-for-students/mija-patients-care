import DashboardLayoutWrapper from "wrappers/DashboardLayoutWrapper";
import { toast } from "react-toastify";
import axios from "axios";

const BaseLoggedView = () => {
  const login = async (email: string) => {
    axios
      .post("api/login", {
        email: email,
        password: "admin123",
      })
      .then((response) => response)
      .then((result) => {
        console.log(result);
        toast.success("Zalogowano");
      })
      .catch((error) => {
        console.log(error);
        toast.error(error.response.data.message);
      });
  };

  return (
    <>
      <DashboardLayoutWrapper
        breadcrumbslinksMap={[
          { label: "Dashboard" },
          { label: "Test", link: "/dashboard/TestVew" },
          { label: "Test2", link: "dashboard/TestVew/test" },
        ]}
      >
        Hello Nicola Tesla
        <button onClick={() => login("admin@asd.pl")}>Success</button>
        <button onClick={() => login("fail")}>Error</button>
      </DashboardLayoutWrapper>
    </>
  );
};
export default BaseLoggedView;
