import { useTranslation } from "react-i18next";
import { FormikConfig, FormikValues } from "formik";
import FormikInput from "components/FormikInput";
import BasicLayout from "layouts/BasicLayout";
import { Link } from "react-router-dom";
import yup from "common/yupLocales";
import { StyledFooter, StyledForm } from "layouts/BasicLayout/BasicLayout.styled";
import Button from "components/Button";
import FormikWithTranslateErrors from "wrappers/FormikWithTranslateErrors";

const LoginView = () => {
  const { t } = useTranslation(["user", "validation"]);

  const loginSchema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().required(),
  });

  const formikConfig: FormikConfig<FormikValues> = {
    initialValues: { email: "", password: "" },
    validationSchema: loginSchema,
    onSubmit: (values) => console.log(values),
  };

  return (
    <BasicLayout title={t("logInMsg")}>
      <FormikWithTranslateErrors formikConfig={formikConfig}>
        <StyledForm>
          <FormikInput name="email" label={t("email")} />
          <FormikInput name="password" label={t("password")} type="password" />
          <Button type="submit">{t("logIn")}</Button>
        </StyledForm>
      </FormikWithTranslateErrors>
      <StyledFooter>
        <Link to="./">{t("forrgotPassword")}</Link>
        <Link to="./">{t("signUp")}</Link>
      </StyledFooter>
    </BasicLayout>
  );
};

export default LoginView;
