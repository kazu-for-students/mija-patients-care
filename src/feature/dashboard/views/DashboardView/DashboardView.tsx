import DashboardLayoutWrapper from "wrappers/DashboardLayoutWrapper";

const DashboardView = () => {
  return (
    <DashboardLayoutWrapper
    breadcrumbslinksMap={[
        { label: "Dashboard" },
        { label: "Test", link: "/dashboard/TestVew" },
        { label: "Test2", link: "dashboard/TestVew/test" },
      ]}
    >
      Hello
    </DashboardLayoutWrapper>
  );
};

export default DashboardView;
