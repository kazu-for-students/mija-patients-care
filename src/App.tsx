import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import { theme } from "theme";
import GlobalStyle from "theme/globalStyles";
import { Normalize } from "styled-normalize";
import Router from "Router";
import { Provider } from "react-redux";
import { store } from "store/store";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <ToastContainer
          position="top-left"
          theme="light"
          autoClose={5000}
          pauseOnHover
          newestOnTop={true}
        />
        <Normalize />
        <GlobalStyle />
        <BrowserRouter>
          <Router />
        </BrowserRouter>
      </ThemeProvider>
    </Provider>
  );
}

export default App;
